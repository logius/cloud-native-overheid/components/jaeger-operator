# Jaeger Operator

Links:
- Chart sources and flags: https://github.com/jaegertracing/helm-charts/tree/main/charts/jaeger-operator
- Releases: https://github.com/jaegertracing/jaeger-operator/releases
- Docs: https://github.com/jaegertracing/jaeger-operator

## Docker rate limiting

To prevent the Docker rate limiting override the image definition.

After the Jaeger-Operator is rolled-out in the cluster, you can install Jaeger into a namespace with custom images.

Sample snippet with custom image:

```yaml
#
# Jaeger instance
#
apiVersion: jaegertracing.io/v1
kind: Jaeger # Custom Resource
metadata:
  name: jaeger-v-sp-demo # Using "v" for volatile, use unique name in cluster to refer to
spec:
  # Optional specify image to prevent 'docker rate image'
  agent:
    image: HARBOR_PROXY/jaegertracing/jaeger-agent:1.17.0
  schema:
    image: HARBOR_PROXY/jaegertracing/jaeger-cassandra-schema:1.17.0
  ingester:
    image: HARBOR_PROXY/jaegertracing/jaeger-ingester:1.17.0
  agent:
    image: HARBOR_PROXY/jaegertracing/jaeger-agent:1.17.0
  collector:
    image: HARBOR_PROXY/jaegertracing/jaeger-collector:1.17.0
  query:
    image: HARBOR_PROXY/jaegertracing/jaeger-query:1.17.0
  spark:
    image: HARBOR_PROXY/jaegertracing/spark-dependencies:1.17.0
  esIndexCleaner:
    image: HARBOR_PROXY/jaegertracing/jaeger-es-index-cleaner:1.17.0
  allInOne:
    image: HARBOR_PROXY/jaegertracing/all-in-one:1.17.0
  strategy: allInOne # https://www.jaegertracing.io/docs/1.17/operator/#allinone-default-strategy
  ...
```
